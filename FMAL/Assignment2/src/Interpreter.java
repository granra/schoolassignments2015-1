import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created by arnar on 2/6/15.
 */
public class Interpreter {

    public static void main (String[] args) {

        Scanner scanner = new Scanner(System.in);
        Stack<String> stack = new Stack<String>();
        Map<String, Integer> variables = new HashMap<String, Integer>();
        String op, val;

        while (scanner.hasNext()) {

            op = scanner.next();

            if (op.equals("ADD"))           add(stack, variables);
            else if (op.equals("SUB"))      sub(stack, variables);
            else if (op.equals("MULT"))     mult(stack, variables);
            else if (op.equals("PRINT"))    print(stack, variables);
            else if (op.equals("ASSIGN"))   assign(stack, variables);
            else if (op.equals("PUSH")) {

                val = scanner.next();
                push(stack, val);

            }
            else {

                System.out.println("Error for operator: " + op);
                System.exit(0);

            }

        }

    }

    public static Integer getInt (String in, Map<String, Integer> variables) {

        Integer out;

        if (isNumber(in))   out = Integer.parseInt(in);
        else /*a variable*/ out = variables.get(in);

        return out;

    }

    public static void add (Stack<String> stack, Map<String, Integer> variables) {

        Integer first, second;

        second = getInt(stack.pop(), variables);
        first = getInt(stack.pop(), variables);

        stack.push((first + second) + "");

    }

    public static void sub (Stack<String> stack, Map<String, Integer> variables) {

        Integer first, second;

        second = getInt(stack.pop(), variables);
        first = getInt(stack.pop(), variables);

        stack.push((first - second) + "");

    }

    public static void mult (Stack<String> stack, Map<String, Integer> variables) {

        Integer first, second;

        second = getInt(stack.pop(), variables);
        first = getInt(stack.pop(), variables);

        stack.push((first * second) + "");

    }

    public static void print (Stack<String> stack, Map<String, Integer> variables) {

        Integer value = getInt(stack.pop(), variables);

        System.out.println(value);

    }

    public static void assign (Stack<String> stack, Map<String, Integer> variables) {

        String var, num;

        num = stack.pop();
        var = stack.pop();

        variables.put(var, Integer.parseInt(num));

    }

    public static void push (Stack<String> stack, String val) {

        stack.push(val);

    }

    public static boolean isNumber (String val) {

        return val.matches("-?\\d+(\\.\\d+)?");

    }

}
