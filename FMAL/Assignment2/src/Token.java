/**
 * Created by arnar on 2/6/15.
 */
public class Token {

    public static enum TokenCode {
        ID, ASSIGN, SEMICOL, INT, PLUS, MINUS,
        MULT, LPAREN, RPAREN, PRINT, END, ERROR
    }

    public String lexeme;
    public TokenCode tCode;

    public Token (String lexeme, TokenCode tCode) {

        this.lexeme = lexeme;
        this.tCode = tCode;

    }

}
