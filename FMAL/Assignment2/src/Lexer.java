import java.util.Scanner;

/**
 * Created by arnar on 2/6/15.
 */
public class Lexer {

    private Scanner scanner;
    private StringBuilder lexeme;
    private StringBuilder buffer;

    public Lexer () {

        scanner = new Scanner(System.in);
        lexeme = new StringBuilder();
        buffer = new StringBuilder();

    }

    public Token nextToken() {

        // clearing lexeme stringBuilder
        lexeme.setLength(0);

        // if the buffer is empty and there is more to read from stdin
        if (buffer.toString().equals("") && scanner.hasNext()) {

            buffer.append(scanner.next());

        }

        // checking if next char in buffer is one of single char tokens
        if (checkSingle(buffer.charAt(0))) {

            lexeme.append(buffer.charAt(0));
            buffer.deleteCharAt(0);

        }
        else {

            // iterating through the buffer until it's empty
            // or I hit one of single char tokens
            while (!buffer.toString().equals("")) {

                if (checkSingle(buffer.charAt(0))) break;

                lexeme.append(buffer.charAt(0));
                buffer.deleteCharAt(0);

            }

        }

        return makeToken(lexeme.toString());

    }

    private Token makeToken(String lex) {

        if (lex.equals("+"))     return new Token(lex, Token.TokenCode.PLUS);
        if (lex.equals("-"))     return new Token(lex, Token.TokenCode.MINUS);
        if (lex.equals("*"))     return new Token(lex, Token.TokenCode.MULT);
        if (lex.equals("end"))   return new Token(lex, Token.TokenCode.END);
        if (lex.equals("print")) return new Token(lex, Token.TokenCode.PRINT);
        if (lex.equals("("))     return new Token(lex, Token.TokenCode.LPAREN);
        if (lex.equals(")"))     return new Token(lex, Token.TokenCode.RPAREN);
        if (lex.equals("="))     return new Token(lex, Token.TokenCode.ASSIGN);
        if (lex.equals(";"))     return new Token(lex, Token.TokenCode.SEMICOL);
        if (checkID(lex))        return new Token(lex, Token.TokenCode.ID);
        if (checkInt(lex))       return new Token(lex, Token.TokenCode.INT);

        return new Token(lex, Token.TokenCode.ERROR);

    }

    private boolean checkInt(String lex) {

        for (char c : lex.toCharArray()) {

            if (!Character.isDigit(c)) return false;

        }

        return true;

    }

    private boolean checkID(String lex) {

        for (char c : lex.toCharArray()) {

            if (!Character.isLetter(c)) return false;

        }

        return true;

    }

    private boolean checkSingle(char in) {

        return  in == '+' || in == '-' || in == '*' || in == ';' ||
                in == '(' || in == ')' || in == '=';

    }

}
