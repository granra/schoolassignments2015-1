import java.util.Stack;

/**
 * Created by arnar on 2/6/15.
 */
public class Parser {

    private Lexer lexer;
    private Token currToken;
    private boolean nextToken;

    public Parser (Lexer lexer) {

        this.lexer = lexer;

        // nextToken tells fetchToken() if we're done looking at
        // the current token
        this.nextToken = true;

    }

    public void parse() {

        statements();

    }

    // Statements -> Statement ; Statements | end
    private void statements() {

        fetchToken();

        if (end()) System.exit(0);

        statement();

        fetchToken();

        if (!semicol()) error();
        else {

            nextToken = true;
            statements();

        }

    }

    // Statement -> id = Expr | print id
    private void statement() {

        if (id()) {

            System.out.println("PUSH " + currToken.lexeme);
            nextToken = true;
            fetchToken();

            if (assign()) {

                nextToken = true;
                expr();
                System.out.println("ASSIGN");

            }
            else error();

        }
        else if (print()) {

            nextToken = true;
            fetchToken();

            if (id()) {

                nextToken = true;
                System.out.println("PUSH " + currToken.lexeme);
                System.out.println("PRINT");

            }
            else error();

            fetchToken();

        }

    }

    // Expr -> Term | Term + Expr | Term - Expr
    private void expr() {

        fetchToken();

        term();

        fetchToken();

        if (plus()) {

            nextToken = true;
            expr();
            System.out.println("ADD");

        }
        else if (minus()) {

            nextToken = true;
            expr();
            System.out.println("SUB");

        }
    }

    // Term -> Factor | Factor * Term
    private void term() {

        factor();

        fetchToken();

        if (mult()) {

            nextToken = true;
            fetchToken();
            term();
            System.out.println("MULT");

        }

    }

    // Factor -> int | id | ( Expr )
    private void factor() {

        if (integer() || id()) {

            nextToken = true;
            System.out.println("PUSH " + currToken.lexeme);

        }
        else if (lparen()) {

            nextToken = true;
            fetchToken();
            expr();

            fetchToken();
            if (!rparen()) error();
            else nextToken = true;
        }
        else error();

    }

    private boolean id()        { return currToken.tCode == Token.TokenCode.ID;      }
    private boolean assign()    { return currToken.tCode == Token.TokenCode.ASSIGN;  }
    private boolean print()     { return currToken.tCode == Token.TokenCode.PRINT;   }
    private boolean plus()      { return currToken.tCode == Token.TokenCode.PLUS;    }
    private boolean minus()     { return currToken.tCode == Token.TokenCode.MINUS;   }
    private boolean mult()      { return currToken.tCode == Token.TokenCode.MULT;    }
    private boolean integer()   { return currToken.tCode == Token.TokenCode.INT;     }
    private boolean lparen()    { return currToken.tCode == Token.TokenCode.LPAREN;  }
    private boolean rparen()    { return currToken.tCode == Token.TokenCode.RPAREN;  }
    private boolean semicol()   { return currToken.tCode == Token.TokenCode.SEMICOL; }
    private boolean end()       { return currToken.tCode == Token.TokenCode.END;     }

    private void error() {

        System.out.println("Syntax error!");
        System.exit(0);

    }

    private void fetchToken() {

        if (nextToken) {

            nextToken = false;
            currToken = lexer.nextToken();

        }
        if (currToken.tCode == Token.TokenCode.ERROR) error();

    }

}
