fun insert(n:real, nil) = [n]
|   insert(n:real, a::lis:real list) =
        if n < a then (n::a::lis)
        else          (a::insert(n, lis));

fun insertsort nil = nil
|   insertsort (a::lis:real list) = insert (a, insertsort lis);
