(* Part 3 *)
(* zip *)
fun zip nil nil = nil
|   zip lis nil = nil
|   zip nil lis = nil
|   zip (a::lis) (b::lis2) = (a,b)::zip lis lis2;

(* greaterThan *)
fun greaterThan nil n       = nil
|   greaterThan (a::lis) n  =
        if a > n then a::(greaterThan lis n)
        else (greaterThan lis n);

(* reduction *)
fun reduction f (a::nil)    = a
|   reduction f (a::b::lis) = reduction (f) (f(a,b)::lis);

(* partition *)
fun partition f nil = (nil, nil)
|   partition f lis =
        let
            fun T fb nil = nil
            |   T fb (a::L) =
                    if fb(a) then   (a :: T fb L)
                    else            (T fb L);
            fun F fb nil = nil
            |   F fb (a::L) =
                    if fb(a) then   (F fb L)
                    else            (a :: F fb L);
        in
            (T f lis, F f lis)
        end;


(* Part 4 *)
(* insert *)
fun insert(n:real, nil) = [n]
|   insert(n:real, a::lis:real list) =
        if n < a then (n::a::lis)
        else          (a::insert(n, lis));

(* insertsort *)
fun insertsort nil = nil
|   insertsort (a::lis:real list) = insert (a, insertsort lis);

(* middle *)
fun middle lis =
    let
        val middle = (length lis) div 2;
        fun itemAt(n, a::L) =
                if n = 0 then   a
                else            itemAt(n-1, L);
    in
        itemAt(middle, lis)
    end;

(* cartesian *)
fun cartesian lis1 lis2 =
    let
        fun c2(x, nil) = nil
        |   c2(x, a::L) = (x, a) :: c2(x, L);
        fun c1(nil, L2) = nil
        |   c1(a::L1, L2) = c2(a, L2) @ c1(L1, L2);
    in
        c1(lis1, lis2)
    end;

(* mymap *)
fun mymap f =
    let
        fun ret nil = nil
        |   ret (a::L) = f(a) :: ret(L);
    in
        ret
    end;

(* test cases *)
(* part 3 *)
zip [1,2,3] ["a","b","c"];
zip [1,2] ["a"];
greaterThan [1,5,3,2,4] 3;
reduction op+ [1,3,5,7,9];
reduction op* [1,3,5,7,9];
partition Char.isLower [#"P",#"a",#"3",#"%",#"b"];
(* part 4 *)
insert (3.3, [1.1, 2.2, 4.4, 5.5]);
insertsort [2.2, 4.4, 5.5, 3.3, 1.1];
middle [1,2,3,4,5];
middle [true, false];
cartesian ["a","b","c"] [1,2];
(mymap (fn x => x*x)) [1,2,3,4];
