fun zip nil nil = nil
|   zip lis nil = nil
|   zip nil lis = nil
|   zip (a::lis) (b::lis2) = (a,b)::zip lis lis2;
