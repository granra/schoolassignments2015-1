fun mymap f =
    let
        fun ret nil = nil
        |   ret (a::L) = f(a) :: ret(L);
    in
        ret
    end;
