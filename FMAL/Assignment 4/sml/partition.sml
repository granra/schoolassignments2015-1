fun partition f nil = (nil, nil)
|   partition f lis =
        let
            fun T fb nil = nil
            |   T fb (a::L) =
                    if fb(a) then   (a :: T fb L)
                    else            (T fb L);
            fun F fb nil = nil
            |   F fb (a::L) =
                    if fb(a) then   (F fb L)
                    else            (a :: F fb L);
        in
            (T f lis, F f lis)
        end;
