fun cartesian lis1 lis2 =
    let
        fun c2(x, nil) = nil
        |   c2(x, a::L) = (x, a) :: c2(x, L);
        fun c1(nil, L2) = nil
        |   c1(a::L1, L2) = c2(a, L2) @ c1(L1, L2);
    in
        c1(lis1, lis2)
    end;
