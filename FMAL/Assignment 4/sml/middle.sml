fun middle lis =
    let
        val middle = (length lis) div 2;
        fun itemAt(n, a::L) =
                if n = 0 then   a
                else            itemAt(n-1, L);
    in
        itemAt(middle, lis)
    end;
