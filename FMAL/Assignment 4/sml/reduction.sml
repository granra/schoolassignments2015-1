fun reduction f (a::nil)    = a
|   reduction f (a::b::lis) = reduction (f) (f(a,b)::lis);
