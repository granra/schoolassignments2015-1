fun greaterThan nil n       = nil
|   greaterThan (a::lis) n  =
        if a > n then a::(greaterThan lis n)
        else (greaterThan lis n);
