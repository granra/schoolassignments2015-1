insertElementAt(Elem, L1, 1, [Elem|L1]).
insertElementAt(Elem, [H|T1], N, [H|T2]) :- insertElementAt(Elem, T1, M, T2), N is M + 1.
