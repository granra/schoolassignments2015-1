mysplit([], [], []).
mysplit([H], [H1], []) :- H = H1, mysplit([], [], []).
mysplit([A,B|T], [H1|T1], [H2|T2]) :- A = H1, B = H2, mysplit(T, T1, T2).
