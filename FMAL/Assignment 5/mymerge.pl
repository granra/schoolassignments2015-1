mymerge([], [], []).
mymerge([], [H2|T2], [H3|T3]) :- H2 = H3, mymerge([], T2, T3).
mymerge([H1|T1], [], [H3|T3]) :- H1 = H3, mymerge(T1, [], T3).
mymerge([H1|T1], [H2|T2], [H3|T3]) :-
    H1 >= H2, H2 = H3,
    mymerge([H1|T1], T2, T3);
    H2 > H1, H1 = H3,
    mymerge(T1, [H2|T2], T3).
