segment_helper([], _).
segment_helper([H|T1], [H|T2]) :- segment_helper(T1, T2).

segment([], _).
segment([H|T1], [H|T2]) :- segment_helper(T1, T2), !.
segment(L1, [_|T2]) :- segment(L1, T2).
