mymerge([], [], []).
mymerge([], [H2|T2], [H3|T3]) :- H2 = H3, mymerge([], T2, T3).
mymerge([H1|T1], [], [H3|T3]) :- H1 = H3, mymerge(T1, [], T3).
mymerge([H1|T1], [H2|T2], [H3|T3]) :-
    H1 >= H2, H2 = H3,
    mymerge([H1|T1], T2, T3);
    H2 > H1, H1 = H3,
    mymerge(T1, [H2|T2], T3).

mysplit([], [], []).
mysplit([H], [H1], []) :- H = H1, mysplit([], [], []).
mysplit([A,B|T], [H1|T1], [H2|T2]) :- A = H1, B = H2, mysplit(T, T1, T2).

mysort([], []).
mysort([I], [I]).
mysort([H1,HH1|T1], [H2,HH2|T2]) :-
    mysplit([H1,HH1|T1], A, B),
    mysort(A, A1),
    mysort(B, B1),
    mymerge(A1, B1, [H2,HH2|T2]).
