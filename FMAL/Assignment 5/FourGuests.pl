% 1.
% a)
married(tom, jane).
married(jane, tom).
rich(steve).
greedy(john).
affair(steve, jane).
affair(jane, steve).
affair(steve, mary).
affair(mary, steve).

hatred(X,Y) :-
    affair(Y, Z),
    married(X,Z).

greed(X,Y) :-
    greedy(X),
    not(rich(X)),
    rich(Y).

murdered(X, Y) :- hatred(X,Y); greed(X, Y).

% b)
% Tom and John
% just run murdered(X,steve).

% c)
rich(john).
