addUpListHelper([], [], _).
addUpListHelper([H|T], [H2|T2], Acc) :-
    Bcc is Acc + H,
    H2 = Bcc,
    addUpListHelper(T, T2, Bcc).

addUpList([], []).
addUpList([H|T], [H2|T2]) :- H = H2, addUpListHelper(T, T2, H).
