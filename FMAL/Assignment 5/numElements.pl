numElements(0, []).
numElements(X, [_|T]) :- numElements(C, T), X is C + 1.
