$(document).ready(function() {
    var redoItem, currentInputBox, currItem;
    var nextFont = "20px Georgia";
    var cursor = new Cursor("Black", 0, 0);
    var started = false;
    var canvas = document.getElementById("PaintWindow");
    drawing.context = canvas.getContext("2d");
    drawing.nextObject = $("input:radio[name=tool]").val();
    drawing.nextLineWidth = $("input:radio[name=thickness]").val();

    $("#PaintWindow").mousedown(function(e) {
        started = true;
        e._x = e.pageX - $("#PaintWindow").offset().left;
        e._y = e.pageY - $("#PaintWindow").offset().top;
        drawing.nextColor = $("#colorscheme").val();
        drawing.context.lineWidth = drawing.nextLineWidth;
        nextFont = $("#fonts").val();
        drawing.context.strokeStyle = drawing.nextColor;
        if(drawing.nextObject === "pencil"){
            currItem = new Pencil(drawing.nextColor, e._x, e._y, drawing.nextLineWidth);
        } else if(drawing.nextObject === "rect"){
            currItem = new Rect(drawing.nextColor, e._x, e._y, 0, 0, drawing.nextLineWidth);
        } else if(drawing.nextObject === "line"){
            currItem = new Line(drawing.nextColor, e._x, e._y, 0, 0, drawing.nextLineWidth);
        } else if(drawing.nextObject === "circle"){
            currItem = new Circle(drawing.nextColor, e._x, e._y, 0, drawing.nextLineWidth);
        } else if(drawing.nextObject ==="text"){
            currItem = new Text(drawing.nextColor, e._x, e._y, "", nextFont);
            generateInbox(e);
        } else if(drawing.nextObject ==="cursor"){
            cursor.x = e._x;
            cursor.y = e._y;
            currItem = cursor;
            cursor.mousedown(e, drawing.context);
            return;
        }
        if(currItem.mousedown) currItem.mousedown(e, drawing.context);
        if(drawing.nextObject != "cursor") drawing.shapes.push(currItem);
    });

    $("#PaintWindow").mousemove(function(e){
        e._x = e.pageX - $("#PaintWindow").offset().left;
        e._y = e.pageY - $("#PaintWindow").offset().top;
        if(started){
            currItem.mousemove(e, drawing.context, canvas);
        }

    });
    $("#PaintWindow").mouseup(function(e){
        if(started){
            started = false;
            drawing.context.clearRect(0, 0, canvas.width, canvas.height);
            drawing.drawAll();
            cursor.target = undefined;
        }
    });

    $("#UndoButton").click(function(){
        if(drawing.shapes.length > 0){
            redoItem = drawing.shapes.pop();
            drawing.context.clearRect(0, 0, canvas.width, canvas.height);
            drawing.drawAll();
        }
    });

    $("#RedoButton").click(function(){
        drawing.shapes.push(redoItem);
        drawing.context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    });

    $("#ClearButton").click(function(){
        while(drawing.shapes.length > 0){
            drawing.shapes.pop();
        }
        drawing.context.clearRect(0, 0, canvas.width, canvas.height);
    });

    $("input:radio[name=tool]").click(function() {
        drawing.nextObject = $(this).val();
    });

    $("input:radio[name=thickness]").click(function() {
        drawing.nextLineWidth = $(this).val();
    });

    $(document).keypress(function(event) {
        if(event.which === 13) {
            if(currentInputBox) {
                currItem.createStaticText(drawing.context, currItem.x, currItem.y, currentInputBox.val());
                currentInputBox.remove();
            }
        }
    });
    function generateInbox(event) {
        if(currentInputBox) {
            currentInputBox.remove();
        }
        var x = event.pageX;
        var y = event.pageY;
        currentInputBox = $("<input />");
        currentInputBox.css("position", "fixed");
        currentInputBox.css("top", y);
        currentInputBox.css("left", x);
        $(".text-spawner").append(currentInputBox);
        currentInputBox.focus();
    }

});
