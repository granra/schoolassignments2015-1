var drawing = {
    shapes: [],
    nextObject: "pencil",
    nextColor: "#000000",
    nextLineWidth: "5",
    context: {},
    drawAll: function() {
        for (var i = 0; i < this.shapes.length; ++i) {
            this.context.strokeStyle = this.shapes[i].color;
            this.context.lineWidth = this.shapes[i].linewidth;
            this.shapes[i].draw(this.context);
        }
    }
};



var Shape = Base.extend({
    constructor: function(color, x, y, linewidth) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.linewidth = linewidth;
    }
});

var Point = Base.extend({
    constructor: function(x, y){
        this.x = x;
        this.y = y;
    }
});

var Pencil = Shape.extend({
    constructor: function(color, x, y, linewidth){
        this.base(color, x, y, linewidth);
        this.points = [];
    },
    draw: function(context){
        context.beginPath();
        for(var i = 0; i < this.points.length; ++i){
            if(i === 0){
                context.moveTo(this.points[0].x, this.points[0].y);
            }
            context.lineTo(this.points[i].x, this.points[i].y);
            context.stroke();
        }
    },
    mousedown: function(ev, context){
        context.beginPath();
        context.moveTo(ev._x, ev._y);
        this.points.push(new Point(ev._x, ev._y));
    },
    mousemove: function(ev, context){
        this.points.push(new Point(ev._x, ev._y));
        context.lineTo(ev._x, ev._y);
        context.stroke();
    },
    checkForContact: function(ev, context){
        for(var i = 0; i < this.points.length; ++i){
            if((ev._x >= this.points[i].x - 5 && ev._x <= this.points[i].x + 5) && (ev._y >= this.points[i].y - 5 && ev._y <= this.points[i].y + 5)){
                return true;
            }
        }
    },
    cursorMove: function(ev, context, canvas, offsetx, offsety){
        offsetx = ev._x - this.points[0].x;
        offsety = ev._y - this.points[0].y;
        for(var i = 0; i < this.points.length; ++i){
            if(this.points[i] != undefined){
                this.points[i].x += offsetx;
                this.points[i].y += offsety;
            }
        }
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    }
});

var Rect = Shape.extend({
    constructor: function(color, x, y, w, h, linewidth) {
        this.base(color, x, y, linewidth);
        this.width = w;
        this.height = h;
    },
    draw: function(context) {
        context.strokeRect(this.x, this.y, this.width, this.height);
    },
    mousemove: function(ev, context, canvas) {
        this.width = ev._x - this.x;
        this.height = ev._y - this.y;
        if(this.width < 0) this.width *= -1;
        if(this.height < 0) this.height *= -1;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
        context.strokeRect(this.x, this.y, this.width, this.height);
    },
    checkForContact: function(ev, context){
        return ((ev._x <= this.x + this.width && ev._x >= this.x) && (ev._y <= this.y + this.height && ev._y >= this.y));
    },
    cursorMove: function(ev, context, canvas, offsetx, offsety){
        this.x = ev._x - offsetx;
        this.y = ev._y - offsety;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    }
});

var Circle = Shape.extend({
    constructor: function(color, x, y, r, linewidth) {
        this.base(color, x, y, linewidth);
        this.radius = r;
    },
    draw: function(context) {
        context.beginPath();
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        context.stroke();
    },
    mousemove: function(ev, context, canvas) {
        this.radius = ev._x - this.x;
        if(this.radius < 0) this.radius *= -1;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
        context.beginPath();
        context.arc(this.x, this.y, this.radius, 0, 2 * Math.PI, false);
        context.stroke();
    },
    checkForContact: function(ev, context){
        return ((ev._x <= this.x + this.radius && ev._x >= this.x - this.radius) && (ev._y <= this.y + this.radius && ev._y >= this.y - this.radius));
    },
    cursorMove: function(ev, context, canvas){
        this.x = ev._x;
        this.y = ev._y;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    }
});

var Line = Shape.extend({
    constructor: function(color, x1, y1, x2, y2, linewidth){
        this.base(color, x1, y1, linewidth);
        this.x2 = x2;
        this.y2 = y2;
        this.width;
        this.height;
    },
    draw: function(context){
        context.beginPath();
        context.moveTo(this.x2, this.y2);
        context.lineTo(this.x, this.y);
        context.stroke();
    },
    mousemove: function(ev, context, canvas) {
        this.x2 = ev._x;
        this.y2 = ev._y;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
        context.beginPath();
        context.moveTo(this.x, this.y);
        context.lineTo(this.x2, this.y2);
        context.stroke();
        context.closePath();
    },
    checkForContact: function(ev, context){
        this.width = this.x - this.x2;
        this.height = this.y - this.y2;
        var tempWidth = this.width;
        var tempHeight = this.height;
        var x = this.x;
        var y = this.y;
        if(tempWidth < 0) {
            tempWidth *= -1;
        } else x -= tempWidth;
        if(tempHeight < 0) {
            tempHeight *= -1;
        } else y -= tempHeight;
        return ((ev._x <= x + tempWidth && ev._x >= x) && (ev._y <= y + tempHeight && ev._y >= y));
    },
    cursorMove: function(ev, context, canvas, offsetx, offsety){
        this.x = ev._x + offsetx;
        this.y = ev._y + offsety;
        this.x2 = this.x + this.width;
        this.y2 = this.y + this.height;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    }
});

var Text = Shape.extend({
    constructor: function(color, x, y, linewidth, font){
        this.base(color, x, y, linewidth);
        this.inputText = "";
        this.font = font;
    },
    draw: function(context){
        this.createStaticText(context, this.x, this.y, this.inputText);
    },
    mousemove: function(){
        return;
    },
    createStaticText: function(context, left, top, text) {
        if(text === undefined){
            text = "";
        }
        context.font = this.font;
        context.fillStyle = this.color;
        context.fillText(text, left, top);
        this.inputText = text;
    },
    checkForContact: function(ev, context){
        var width = context.measureText(this.inputText).width;
        var height = 20;
        return ((ev._x <= this.x + width && ev._x >= this.x) && (ev._y >= this.y - height && ev._y <= this.y));
    },
    cursorMove: function(ev, context, canvas, offsetx, offsety){
        this.x = ev._x - offsetx;
        this.y = ev._y - offsety;
        context.clearRect(0, 0, canvas.width, canvas.height);
        drawing.drawAll();
    }
});

var Cursor = Shape.extend({
    constructor: function(color, x, y){
        this.base(color, x, y);
        this.target;
    },
    mousedown: function(ev, context){
        for(var i = drawing.shapes.length; i >= 0; --i){
            if(drawing.shapes[i] != undefined){
                if(drawing.shapes[i].checkForContact(ev, context)){
                    this.target = drawing.shapes[i];
                    this.x = ev._x - drawing.shapes[i].x;
                    this.y = ev._y - drawing.shapes[i].y;
                    return;
                }
            }
        }
        return this;
    },
    mousemove: function(ev, context, canvas){
        if(this.target != undefined){
            this.target.cursorMove(ev, context, canvas, this.x, this.y);
        }
    }
});
